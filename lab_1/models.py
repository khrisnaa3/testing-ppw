from django.db import models

class Post(models.Model):
    nama_kegiatan = models.CharField(max_length=27, default='nama kegiatan')
    tempat = models.CharField(max_length=27, default='tempat kegiatan')
    kategori = models.CharField(max_length=27, default='kategori')
    waktu = models.CharField(max_length=27, default='00:00')
    tanggal = models.CharField(max_length=27, default='mm/dd/yyyy')