from django import forms
from lab_1.models import Post

class HomeForm(forms.Form):
    error_messages = {
        'required': 'Tolong isikan kolom ini'
    }
    attrs = {
        'class': 'form-control'
    }

    nama_kegiatan = forms.CharField(label='Nama Kegiatan', required=True, max_length=27, widget=forms.TextInput(attrs=attrs))
    tempat = forms.CharField(label='Tempat', required=True, max_length=27, widget=forms.TextInput(attrs=attrs))
    kategori = forms.CharField(label='Kategori Kegiatan', required=True, max_length=27,
                               widget=forms.TextInput(attrs=attrs))
    waktu = forms.CharField(label='Waktu', required=True,
                           widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'time'}))
    tanggal = forms.CharField(label='Tanggal', required=True,
                            widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'date'}))
