from django.urls import path
from . import views

#url for app
urlpatterns = [
    path('', views.beranda, name="beranda"),
    path('video', views.video, name="video"),
    path('register', views.register, name="register"),
    path('form', views.form, name="form"),
    path('form-success', views.post_form, name="form-success"),
    path('form-table', views.form_table, name="form-table"),
    path('form-table/hapus', views.delete_db, name="hapus")
]
