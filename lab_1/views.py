from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import HomeForm
from .models import Post


# Create your views here.
def beranda(request):
    return render(request, 'beranda.html')

def video(request):
    return render(request, 'video.html')

def register(request):
    return render(request, 'register.html')

def form(request):
    response = {'form_regis': HomeForm}
    return render(request, 'forms.html', response)

def delete_db(request):
    Post.objects.all().delete()
    return render(request, 'hapus.html')


response={}
def post_form(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = HomeForm(request.POST or None)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            response['nama_kegiatan'] = request.POST['nama_kegiatan']
            response['tempat'] = request.POST['tempat']
            response['kategori'] = request.POST['kategori']
            response['waktu'] = request.POST['waktu']
            response['tanggal'] = request.POST['tanggal']
            form_save = Post(nama_kegiatan=response['nama_kegiatan'], tempat=response['tempat'],
                             kategori=response['kategori'], waktu=response['waktu'], tanggal=response['tanggal'])
            form_save.save()
            return render(request, "form_success.html", response)
        else:
            response['form'] = form
            return render(request, "forms.html", response)
    # if a GET (or any other method) we'll create a blank form
    else:
        return HttpResponseRedirect('/form')

def form_table(request):
    form_tb = Post.objects.all()
    response['form_tb'] = form_tb
    html = 'form_table.html'
    return render(request, html , response)





